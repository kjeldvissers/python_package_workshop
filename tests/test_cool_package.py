import pytest
from cool_package import mathfunctions as mf

@pytest.mark.parametrize("nr1, nr2, expected", [(1,2,3), (3.5, 2.1, 5.6), (10,300,310)])
def test_add_numbers_nr(nr1, nr2, expected):
    """
    Tests if function gives expected number
    """
    result = mf.add_numbers(x = nr1, y = nr2)
    assert result == expected

@pytest.mark.parametrize("inp1, inp2, expected", [("hoi","hallo","hoihallo")])
def test_add_numbers_str(inp1, inp2, expected):
    """
    Tests if function gives expected string
    """
    result = mf.add_numbers(x = inp1, y = inp2)
    assert result == expected
    
       