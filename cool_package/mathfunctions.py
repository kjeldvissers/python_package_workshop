
from numbers import Number

def add_numbers(x:Number, y:Number) -> Number:
    """This function adds x and y together and returns the outcome

    Args:
        x (Number): first number we want to add
        y (Number): the scond number we want to add

    Returns:
        Number: the outcome of the addition of the input x and y
    """
    return x+y

def multiply_numbers(x: int, y:int):
    """_summary_

    Args:
        x (int): _description_
        y (int): _description_

    Returns:
        _type_: _description_
    """
    return x*y

def exponentiation(x,y):
    return x**y

def get_modulus(x,y):
    return x%y