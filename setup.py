from platform import python_version
from setuptools import setup, find_packages

setup(
        name = "coolpackage",
        version = "0.1.0",
        python_version = '>=3.9',
        description= 'example', 
        author='Kjeld Vissers',
        author_email='k.vissers@cmotions.nl',
        packages=find_packages()
    )
